'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend'),
    Schema = mongoose.Schema;

var AdSchema = new Schema({
    subject:{type:String,required: 'cannot be blank'},
    catId:{type:String}, //refer to category model
    updateTime:{type:String},
    price:{type:String},
    contacter:{type:String},
    phone:{type:String},
    email:{type:String},
    otherPhone:{type:String},
    area:{type:String},
    position:{type:String},//longitude and latitude.
    zone:{type:String},
    content:{type:String},
    status:{
        type:Number,
        default: 0
    } //0=unassigned(open),1=unsolved,2=Pending,3=suspended,9=solved
});

/**
 * Validations
 */

AdSchema.path('subject').required(true, 'subject cannot be blank');

/**
 * Methods
 */

AdSchema.methods = {};

/**
 * Statics
 */

AdSchema.statics = {};

mongoose.model('Ad', AdSchema,'ads');
