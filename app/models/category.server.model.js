/**
 * Created by gmail on 24/11/14.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var categorySchema = new Schema({
    id: String,
    name: String,
    type: String,
    parentId: String,
    value: String,
    desc:String,
    remark:String,
    items:Array
});

var Category = mongoose.model('Category',categorySchema);
