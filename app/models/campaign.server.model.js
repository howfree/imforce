'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend'),
    Schema = mongoose.Schema;

var CampaignSchema = new Schema({
    name:{type:String,required: 'cannot be blank'},
    type:{type:String},
    description:{type:String},
    createTime:{
        type:String,
        default: Date.now
    },
    startTime:{
        type:String
    },
    endTime:{
        type:String
    },
    creator:{type:String},
    remark:{type:String},
    thumbnail:{type:String},
    ad:{type:String},
    status:{
        type:Number,
        default: 0
    }
});

/**
 * Validations
 */

CampaignSchema.path('name').required(true, 'name cannot be blank');

/**
 * Methods
 */

CampaignSchema.methods = {};

/**
 * Statics
 */

CampaignSchema.statics = {};

mongoose.model('Campaign', CampaignSchema,'campaigns');
