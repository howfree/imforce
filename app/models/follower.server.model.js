'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var FollowerSchema = new Schema({
    imId: {
        type: String,
        required: 'cannot be blank'
    },
    nickName: {type: String},
    city: {type: String},
    province: {type: String},
    country: {type: String},
    sex: {type: String},
    img: {type: String},
    subscribe_time: {
        type: String,
        default: Date.now
    },
    remark: {type: String},
    messages : [{ type: Schema.Types.ObjectId, ref: 'Message' }]
});

mongoose.model('Follower', FollowerSchema);
