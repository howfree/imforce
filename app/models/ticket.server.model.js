'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend'),
    Schema = mongoose.Schema;

var TicketSchema = new Schema({
    subject:{type:String,required: 'cannot be blank'},
    target:{type:String}, //generally it's an imId of a follower
    type:{type:String},
    content:{type:String},
    requestedTime:{
        type:String,
        default: Date.now
    },
    requester:{
        type:String,
        ref: 'User'
    },
    receivedTime:{
        type:String,
        default: Date.now
    },
    receiver:{type:String},
    priority:{type:String},
    groupId:{type:Number},//agent group
    remark:{type:String},
    status:{
        type:Number,
        default: 0
    }, //0=unassigned(open),1=unsolved,2=Pending,3=suspended,9=solved
    messages : [{ type: Schema.Types.ObjectId, ref: 'Message' }]
});

/**
 * Validations
 */

TicketSchema.path('subject').required(true, 'subject cannot be blank');

/**
 * Methods
 */

TicketSchema.methods = {};

/**
 * Statics
 */

TicketSchema.statics = {};

mongoose.model('Ticket', TicketSchema,'tickets');
