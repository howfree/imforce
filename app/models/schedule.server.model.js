var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var scheduleSchema = new Schema({
    name: {type: String},//sync follower or group
    type: {type: Number},//1=manual,0=automatic
    desc:{type: String},
    remark:{type: String},
    time:{
        type: String,
        default: Date.now
    }
});

var Schedule = mongoose.model('Schedule',scheduleSchema);
