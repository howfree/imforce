'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend'),
    Schema = mongoose.Schema;

var MessageSchema = new Schema({
    toUserId:{type:String, required: 'cannot be blank'},
    _toUser:{type:String, ref: 'Follower'},
    fromUserId:{type:String, required: 'cannot be blank'},
    _fromUser:{type:String, ref: 'Follower'},
    createTime:{
        type:String,
        default: Date.now
    },
    msgType:{type:String},
    msgId:{
        type:String
    },
    status:{
        type:String,
        default:'0'
    }
});

/**
 * Validations
 */

MessageSchema.path('fromUserId').required(true, 'fromUserId cannot be blank');
MessageSchema.path('toUserId').required(true, 'toUserId cannot be blank');



/**
 * Methods
 */

MessageSchema.methods = {};

/**
 * Statics
 */

MessageSchema.statics = {};



var TextMessageSchema = MessageSchema.extend({
    content : String
});




var ImageMessageSchema = MessageSchema.extend({
    picUrl : String,
    mediaId: String
});


var VoiceMessageSchema = MessageSchema.extend({
    mediaId : String,
    format : String
});


var VideoMessageSchema = MessageSchema.extend({
    mediaId : String,
    thumbMediaId : String
});

var LocationMessageSchema = MessageSchema.extend({
    locationX : String,
    locationY : String,
    label : String,
    scale : String
});

var LinkMessageSchema = MessageSchema.extend({
    title : String,
    description : String,
    url : String,
    locationX : String,
    locationY : String,
    precision : String
});


var EventMessageSchema = MessageSchema.extend({
    content : String,
    event : String

});


mongoose.model('Message', MessageSchema,'messages');
mongoose.model('TextMessage', TextMessageSchema,'messages');
mongoose.model('ImageMessage', ImageMessageSchema,'messages');
mongoose.model('VoiceMessage', VoiceMessageSchema,'messages');
mongoose.model('VideoMessage', VideoMessageSchema,'messages');
mongoose.model('LocationMessage', LocationMessageSchema,'messages');
mongoose.model('LinkMessage', LinkMessageSchema,'messages');
mongoose.model('EventMessage', EventMessageSchema,'messages');
