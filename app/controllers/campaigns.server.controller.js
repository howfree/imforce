'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    resJson = require('./../lib/tools.js').resJson,
    _ = require('lodash'),
    Campaign = mongoose.model('Campaign');

/**
 * List of campaigns
 */
exports.list = function(req, res) {
    var perPage = req.body.s || 20,
        page = req.body.p || 1;

    Campaign.find().skip((page-1) * perPage).limit(perPage).exec(function (err, campaigns) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            var pagination = {};
            pagination.total = campaigns.length;
            pagination.pageSize = perPage;
            pagination.pageIndex = page;

            res.json(resJson(null, campaigns, pagination));
        }
    });
};


/**
 * Create a campaign
 */
exports.create = function(req, res) {
    var campaign = new Campaign(req.body);
    campaign.creator = req.user._id;
    campaign.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(campaign);
        }
    });
};

/**
 * Show the current Campaign
 */
exports.read = function(req, res) {
    Campaign.findById(req.params.id,function(err, campaign) {
        if (err) return next(err);
        if (!campaign) return next(new Error('Failed to load campaign ' + id));
        req.campaign = campaign;
        res.json(campaign);
    });
};

/**
 * Update a campaign
 */
exports.update = function(req, res) {
    var campaign = req.campaign;

    campaign = _.extend(campaign, req.body);

    campaign.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(campaign);
        }
    });
};

/**
 * Delete a campaign
 */
exports.delete = function(req, res) {
    Campaign.findByIdAndRemove(req.params.id,function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json('ok');
        }
    });
};

