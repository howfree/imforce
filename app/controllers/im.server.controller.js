'use strict';
var WechatAPI = require('wechat-api'),
    wechat = require('wechat'),
    mongoose = require('mongoose'),
    fs = require('fs'),
    Grid = require('gridfs-stream'),
    _ = require('lodash'),
    errorHandler = require('./errors.server.controller'),
    config = require('./../../config/config'),
    thenjs = require('thenjs'),
    Q = require('q'),
    logger = require('./../../config/log').logger;

var Follower = mongoose.model('Follower'),
    Category = mongoose.model('Category'),
    Campaign = mongoose.model('Campaign'),
    Schedule = mongoose.model('Schedule'),
    TextMessage = mongoose.model('TextMessage'),
    ImageMessage = mongoose.model('ImageMessage');
//wechat access object
var api = new WechatAPI(config.wechat.appId, config.wechat.appSecret);

// handle all the inbound service
exports.inbound = wechat(config.wechat.token, wechat.text(function (message, req, res, next) {
    // message为文本内容
    // { ToUserName: 'gh_d3e07d51b513',
    // FromUserName: 'oPKu7jgOibOA-De4u8J2RuNKpZRw',
    // CreateTime: '1359125035',
    // MsgType: 'text',
    // Content: 'http',
    // MsgId: '5837397576500011341' }

    //save and put into queue

    var input = (message.Content || '').trim();

    if (input === 'login') {
        res.reply([{
            title: '登陆页面',
            description: '去登陆',
            picurl: config.wechat.domain + '/assets/images/qrcode.jpg',
            url: config.wechat.domain + '/login'
        }]);

        return;
    }

    else if (input === 'fiat') {
        res.reply([{
            title: 'FIAT mini cooper released',
            description: 'Get MINI Cooper expert reviews, new and used Cooper prices and ratings',
            picurl: 'http://www.wvl.co.uk/wp-content/uploads/2014/03/mini-vs-fiat-featured-660x300.jpg',
            url: 'http://ad.flexitive.com/DubilW2ZTY3'
        }]);

        return;
        //return res.reply('show you all the menus');
    }
    else {
        //initialized text.
        var textMessage = new TextMessage({
            msgId: message.MsgId,
            toUserId: message.ToUserName,
            fromUserId: message.FromUserName,
            msgType: message.MsgType,
            content: message.Content
        });

        //retrieve two users,then put them into collection.
        thenjs(function (cont) {
            Follower.findOne({imId: textMessage.fromUserId}, function (err, fromUser) {
                cont(null, fromUser);
            });

        }).then(function (cont, fromUser) {
            if (fromUser)textMessage._fromUser = fromUser._id;
            cont(null);
        }).then(function (cont) {
            Follower.findOne({imId: textMessage.toUserId}, function (err, toUser) {
                cont(null, toUser);
            });
        }).then(function (cont, toUser) {
            if (toUser) textMessage._toUser = toUser._id;
            cont(null);
        }).then(function (cont) {
            //save text.
            textMessage.save(function (err) {
                // return related campaign or null
                var regex = new RegExp(textMessage.content, "i"),
                query = {name: regex };
                Campaign.findOne(query,function(err,campaign){
                    if(campaign){
                        res.reply([{
                            title: campaign.name,
                            description:campaign.description,
                            picurl: campaign.thumbnail,
                            url: campaign.ad
                        }]);

                        return;
                    }
                    else{
                        return res.reply('');
                    }
                });

            });

        }).fail(function(cont,err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        });

    }


}).image(function (message, req, res, next) {
    // message为图片内容
    // { ToUserName: 'gh_d3e07d51b513',
    // FromUserName: 'oPKu7jgOibOA-De4u8J2RuNKpZRw',
    // CreateTime: '1359124971',
    // MsgType: 'image',
    // PicUrl: 'http://mmsns.qpic.cn/mmsns/bfc815ygvIWcaaZlEXJV7NzhmA3Y2fc4eBOxLjpPI60Q1Q6ibYicwg/0',
    // MediaId: 'media_id',
    // MsgId: '5837397301622104395' }

    var gfs = new Grid(mongoose.connection.db, mongoose.mongo);
    //initialized text.
    var imageMessage = new ImageMessage({
        msgId: message.MsgId,
        toUserId: message.ToUserName,
        fromUserId: message.FromUserName,
        msgType: message.MsgType,
        picUrl: message.picUrl,
        mediaId: message.mediaId
    });

    thenjs(function (cont) {
        //save image into mongodb.
        // streaming to gridfs
        //filename to store in mongodb
        var writestream = gfs.createWriteStream({
            filename: message.PicUrl
        });

        fs.createReadStream(config.file.path).pipe(writestream);

        writestream.on('close', function (file) {
            // do something with `file`
            console.log(file.filename + 'Written To DB');

        });

    }).then(function (cont) {//retrieve two users,then put them into collection.
        Follower.findOne({imId: imageMessage.fromUserId}, function (err, fromUser) {
            cont(null, fromUser);
        });

    }).then(function (cont, fromUser) {
        imageMessage._fromUser.push(fromUser);
        cont(null);
    }).then(function (cont) {
        Follower.findOne({imId: imageMessage.toUserId}, function (err, toUser) {
            cont(null, toUser);
        });
    }).then(function (cont, toUser) {
        imageMessage._toUser.push(toUser);
        cont(null);
    }).then(function (cont) {
        //save text.
        imageMessage.save(function (err) {
            return res.reply('');
        });

    }).fail(function(cont,err){
        return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    });

}).voice(function (message, req, res, next) {
    // message为音频内容
    // { ToUserName: 'gh_d3e07d51b513',
    // FromUserName: 'oPKu7jgOibOA-De4u8J2RuNKpZRw',
    // CreateTime: '1359125022',
    // MsgType: 'voice',
    // MediaId: 'OMYnpghh8fRfzHL8obuboDN9rmLig4s0xdpoNT6a5BoFZWufbE6srbCKc_bxduzS',
    // Format: 'amr',
    // MsgId: '5837397520665436492' }
}).video(function (message, req, res, next) {
    // message为视频内容
    // { ToUserName: 'gh_d3e07d51b513',
    // FromUserName: 'oPKu7jgOibOA-De4u8J2RuNKpZRw',
    // CreateTime: '1359125022',then = require('thenjs')
    // MsgType: 'video',
    // MediaId: 'OMYnpghh8fRfzHL8obuboDN9rmLig4s0xdpoNT6a5BoFZWufbE6srbCKc_bxduzS',
    // ThumbMediaId: 'media_id',
    // MsgId: '5837397520665436492' }
}).location(function (message, req, res, next) {
    // message为位置内容
    // { ToUserName: 'gh_d3e07d51b513',
    // FromUserName: 'oPKu7jgOibOA-De4u8J2RuNKpZRw',
    // CreateTime: '1359125311',
    // MsgType: 'location',
    // Location_X: '30.283950',
    // Location_Y: '120.063139',
    // Scale: '15',
    // Label: {},
    // MsgId: '5837398761910985062' }/home/lastsky/workspace/jsgen
}).link(function (message, req, res, next) {
    // message为链接内容
    // { ToUserName: 'gh_d3e07d51b513',
    // FromUserName: 'oPKu7jgOibOA-De4u8J2RuNKpZRw',
    // CreateTime: '1359125022',
    // MsgType: 'link',
    // Title: '公众平台官网链接',
    // Description: '公众平台官网链接',
    // Url: 'http://1024.com/',
    // MsgId: '5837397520665436492' }
}).event(function (message, req, res, next) {
    // message为事件内容
    // { ToUserName: 'gh_d3e07d51b513',
    // FromUserName: 'oPKu7jgOibOA-De4u8J2RuNKpZRw',
    // CreateTime: '1359125022',
    // MsgType: 'event',
    // Event: 'LOCATION',
    // Latitude: '23.137466',
    // Longitude: '113.352425',
    // Precision: '119.385040',
    // MsgId: '5837397520665436492' }

    if ((message.MsgType === 'event') && (message.Event === 'subscribe')) {
        var refillStr = '<a href=\'http://your_IP/weixin/refill?weixinId=' + message.FromUserName + '\'>1. 点击记录团队充值</a>';
        var consumeStr = '<a href=\'http://your_IP/weixin/consume?weixinId=' + message.FromUserName + '\'>2. 点击记录团队消费</a>';
        var deleteStr = '<a href=\'http://your_IP/weixin/delete?weixinId=' + message.FromUserName + '\'>3. 点击回退记录</a>';
        var historyStr = '<a href=\'http://your_IP/weixin/history?weixinId=' + message.FromUserName + '\'>4. 点击查询历史记录</a>';

        var emptyStr = '          ';
        var replyStr = '感谢你的关注！' + '\n' + emptyStr + '\n' + refillStr + '\n' + emptyStr + '\n' + consumeStr + '\n' + emptyStr + '\n' + deleteStr + '\n' + emptyStr + '\n' + historyStr;
        res.reply(replyStr);
    }
}));

//retrieve followers from wechat
exports.fetchFollowers = function (req, res) {
    // replaced Q with thenjs.
    //getFollowerIdsFromWechat().then(function (result) {
    //
    //    var openIds = result.data.openid;
    //    var promises = [];
    //
    //    openIds.forEach(function (openId) {
    //        var deferred = Q.defer();
    //        //check whether it exists in mongodb
    //        getFollowerFromDB(openId).then(function(follower){
    //            if(follower==null){
    //                //retreive from  wechat,then save it.
    //                getFollowerFromWechat(openId).then(function (follower) {
    //                    createFollower(follower).then(function (follower) {
    //                        deferred.resolve(follower);
    //                    }).done();
    //                }).done();
    //
    //            }
    //            else{
    //                deferred.resolve(null);
    //            }
    //
    //        }).done();
    //        promises.push(deferred.promise);
    //    });
    //
    //
    //    Q.all(promises).then(function (followers) {
    //        var newFollowers= [];
    //
    //        //exclude empty data.
    //        followers.forEach(function(follower){
    //           if(follower!=null){
    //               newFollowers.push(follower);
    //           }
    //        });
    //
    //        res.json(newFollowers);
    //    });
    //
    //
    //}).done();

    thenjs(function (cont) {
        api.getFollowers(function (err, result) {
            cont(null, result);
        });
    }).then(function (cont, result) {
        var openIds = result.data.openid;
        //check whether it exists one by one,then save or skip it.
        var newFollowers = [];
        thenjs.eachSeries(openIds, function (cont2, openId) {
            Follower.findOne({imId: openId}, function (err, follower) {
                if (null === follower) {
                    thenjs(function (cont3) {
                        api.getUser(openId, function (err, follower) {
                            cont3(null, follower);
                        });
                    }).then(function (cont3, follower) {
                        Follower.create({
                            imId: follower.openid,
                            nickName: follower.nickname,
                            city: follower.city,
                            province: follower.province,
                            country: follower.country,
                            sex: follower.sex,//2=female,1=male
                            img: follower.headimgurl,
                            createTime: follower.subscribe_time,
                            remark: follower.remark
                        }, function (err, follower) {
                            newFollowers.push(follower);
                            cont2(null, follower);
                        });
                    });

                }
            });
        }).fin(function (cont2) {//all方法废弃
            cont(null, newFollowers);
        });
    }).then(function (cont, newFollowers) {
        Schedule.create({
            name: 'syncFollower',
            type: 1,
            remark: newFollowers.length || 0
        }, function (err, schedule) {
            res.json(newFollowers);
        });

    }).fail(function(cont,err){
        return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    });
};


// retrieve all groups from wechat.
exports.fetchGroups = function (req, res) {
    // retrieve all the groups
    thenjs(function (cont) {
        api.getGroups(function (err, data) {
            cont(null, data.groups);
        });
    }).then(function (cont, groups) {
        //check whether it exists one by one,then save or skip it.
        var newgroups = [];
        thenjs.eachSeries(groups, function (cont2, group) {

            Category.find({name: group.name}, function (err, cats) {
                if (cats.length === 0) {
                    thenjs(function (cont3) {
                        //newId = maxId + 1
                        Category.findOne({parentId: '6000'}).sort('-id').limit(1).exec(function (err, cat) {
                            var newId = 6000000001;

                            if (cat) newId = parseInt(cat.id) + 1;

                            cont3(null, newId);
                        });
                    }).then(function (cont3, newId) {
                        //save group.
                        Category.create({
                            id: newId.toString(),// max value +1
                            name: group.name,
                            type: '0',
                            parentId: '6000',
                            value: group.id,
                            desc: '',
                            remark: ''
                        }, function (err, cat) {
                            newgroups.push(cat);
                            cont2();
                        });
                    });


                } else {
                    cont2();
                }
            });


        }).fin(function (cont2) {//all方法废弃
            cont(null, newgroups);
        });

    }).then(function (cont, newgroups) {
        Schedule.create({
            name: 'syncGroup',
            type: 1,
            remark: newgroups.length || 0
        }, function (err, schedule) {
            res.json(newgroups);
        });

    }).fail(function(cont,err){
        return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    });
};
