'use strict';
/**
 * Module dependencies.
 */
var WechatAPI = require('wechat-api'),
    mongoose = require('mongoose'),
    thenjs = require('thenjs'),
    errorHandler = require('./errors.server.controller'),
    TextMessage = mongoose.model('TextMessage'),
    Message = mongoose.model('Message'),
    Campaign = mongoose.model('Campaign'),
    Follower = mongoose.model('Follower'),
    resJson = require('./../lib/tools.js').resJson,
    _ = require('lodash'),
    config = require('./../../config/config'),
    logger = require('./../../config/log').logger;

/**
 * new message
 */
exports.create = function(req, res) {
    var textMessage = new TextMessage(req.body);
    textMessage.fromUserId = config.wechat.originID;

    //retrieve two users,then put them into collection.
    thenjs(function(cont){
        Follower.findOne({imId:textMessage.fromUserId}, function(err,fromUser){
            if(err){
                cont(err);
            }
            else{
                cont(null,fromUser);
            }
        });

    }).then(function(cont,fromUser){

        if(fromUser) textMessage._fromUser =  fromUser._id;
        cont(null);
    }).then(function(cont){
        Follower.findOne({imId:textMessage.toUserId},function(err, toUser){
            if(err){
                cont(err);
            }
            else{
                cont(null,toUser);
            }
        });
    }).then(function(cont,toUser){
        if(toUser) textMessage._toUser = toUser._id;
        cont(null);
    }).then(function(cont){
        //save text.
        textMessage.save(function (err) {
            if (err) cont(err);
            // check campaign or faq,then send news message.
            //example:@campaign=xxx or @faq=xxx
            if(textMessage.content.indexOf('=')<0){
                //wechat access object
                var api = new WechatAPI(config.wechat.appId, config.wechat.appSecret);
                api.massSendText(textMessage.content, [textMessage.toUserId], function(err,result){
                    if (err) cont(err);
                    if(result) cont(null,textMessage);
                });
            }
            else{
                //news message
                var autos = textMessage.content.indexOf('=');
                var campaignId =  autos[1];
                Campaign.findOne({id:campaignId},function(err, campaign){
                    if(err){
                        cont(err);
                    }
                    else{
                        var articles = [
                            {
                                "title":campaign.name,
                                "description":campaign.description,
                                "url":campaign.ad,
                                "picurl":campaign.thumbnail
                            }];

                        api.sendNews(textMessage.toUserId, articles, function(err,result){
                            if (err) cont(err);
                            if(result) cont(null,textMessage);

                        });

                    }
                });

            }

        });
    }).then(function(cont){
        res.json(textMessage);
    }).then(function(cont,err){
        logger.error(err);
        return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    });

};


exports.list = function(req, res) {
    var perPage = req.body.s || 20,
        page = req.body.p,
        query = {},
        status =req.query.status,
        direction = req.query.direction,
        fromUserId = req.query.fromUserId;

    //build query parameters
    if(status) query.status = status;
    if('0'===direction) query.toUserId = config.wechat.originID;
    if(fromUserId && ''!==fromUserId) query.$or =[{fromUserId:fromUserId},{toUserId:fromUserId}];

    if(page){ // retrieve messages with pagination
        Message.find(query).populate('_fromUser', 'img nickName imId').skip((page-1) * perPage).limit(perPage).exec(function (err, messages) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                var pagination = {};
                pagination.total = messages.length;
                pagination.pageSize = perPage;
                pagination.pageIndex = page;

                res.json(resJson(null, messages, pagination));
            }
        });
    }
    else{
        Message.find(query).populate('_fromUser', 'img nickName imId').exec(function (err, messages) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.json(resJson(null, messages, null));
            }
        });
    }

};
