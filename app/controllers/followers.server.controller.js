'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Follower = mongoose.model('Follower'),
    resJson = require('./../lib/tools.js').resJson,
    _ = require('lodash');


/**
 * Create a follower
 */
exports.create = function(req, res) {
    var follower = new Follower(req.body);
    follower.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(follower);
        }
    });
};

/**
 * Show the current Follower
 */
exports.read = function(req, res) {
    res.json(req.follower);
};


/**
 * Update a follower
 */
exports.update = function(req, res) {
    var follower = req.follower;

    follower = _.extend(follower, req.body);

    follower.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(follower);
        }
    });
};

/**
 * Delete a follower
 */
exports.delete = function(req, res) {
    var follower = req.follower;

    follower.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(follower);
        }
    });
};

/**
 * List of followers
 */
exports.list = function(req, res) {
    var perPage = req.body.s || 20,
        page = req.body.p || 1;


    Follower.find().skip((page-1) * perPage).limit(perPage).exec(function (err, followers) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            var pagination = {};
            pagination.total = followers.length;
            pagination.pageSize = perPage;
            pagination.pageIndex = page;

            res.json(resJson(null, followers, pagination));
        }
    });
};

/**
 * Follower middleware
 */
exports.followerByID = function(req, res, next, id) {
    Follower.findById(id,function(err, follower) {
        if (err) return next(err);
        if (!follower) return next(new Error('Failed to load follower ' + id));
        req.follower = follower;
        next();
    });
};


