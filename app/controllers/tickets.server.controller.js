'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    resJson = require('./../lib/tools.js').resJson,
    _ = require('lodash'),
    Ticket = mongoose.model('Ticket');

/**
 * List of tickets
 */
exports.list = function(req, res) {
    var perPage = req.body.s || 20,
        page = req.body.p || 1;

    Ticket.find().populate('requester','email username').skip((page-1) * perPage).limit(perPage).exec(function (err, tickets) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            var pagination = {};
            pagination.total = tickets.length;
            pagination.pageSize = perPage;
            pagination.pageIndex = page;

            res.json(resJson(null, tickets, pagination));
        }
    });
};


/**
 * Create a ticket
 */
exports.create = function(req, res) {
    var ticket = new Ticket(req.body);
    ticket.requester = req.user._id;
    ticket.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(ticket);
        }
    });
};

/**
 * Show the current Ticket
 */
exports.read = function(req, res) {
    res.json(req.ticket);
};

/**
 * Update a ticket
 */
exports.update = function(req, res) {
    var ticket = req.ticket;

    ticket = _.extend(ticket, req.body);

    ticket.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(ticket);
        }
    });
};

/**
 * Delete a ticket
 */
exports.delete = function(req, res) {
    var ticket = req.ticket;

    ticket.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(ticket);
        }
    });
};


/**
 * Ticket middleware
 */
exports.ticketByID = function(req, res, next, id) {
    Ticket.findById(id,function(err, ticket) {
        if (err) return next(err);
        if (!ticket) return next(new Error('Failed to load ticket ' + id));
        req.ticket = ticket;
        next();
    });
};
