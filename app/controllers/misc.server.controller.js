'use strict';
var mongoose = require('mongoose');

exports.scheduleList = function (req, res) {
    var Schedule = mongoose.model('Schedule');
    Schedule.find({},function(err,schedules){
        res.json(schedules);
    });
};
