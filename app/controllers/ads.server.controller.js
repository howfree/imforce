'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    thenjs = require('thenjs'),
    resJson = require('./../lib/tools.js').resJson,
    Ad = mongoose.model('Ad'),
    logger = require('./../../config/log').logger;

//all the categories
exports.list = function(req, res) {
    var perPage = req.body.s || 20,
        page = req.body.p || 1;

    Ad.find().skip((page-1) * perPage).limit(perPage).exec(function (err, ads) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            var pagination = {};
            pagination.total = ads.length;
            pagination.pageSize = perPage;
            pagination.pageIndex = page;

            res.json(resJson(null, ads, pagination));
        }
    });
};

exports.create = function(req, res) {

};
