'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    thenjs = require('thenjs'),
    errorHandler = require('./errors.server.controller'),
    resJson = require('./../lib/tools.js').resJson,
    Category = mongoose.model('Category'),
    _ = require('lodash'),
    logger = require('./../../config/log').logger;


exports.fullList = function(req, res) {
    var parentId = req.query.parentId;
    var list =[];
    thenjs(function(cont) {
        //first level
        Category.find({parentId:parentId},function (err, cats) {
            if(err){
                cont(err);
            }
            else{
                cont(null,cats);
            }
        });
    }).then(function(cont,cats){
        //second level
        var catList = [];
        thenjs.each(cats, function (cont2, cat) {
            //put sub-catgory
            Category.find({parentId: cat.id}).exec(function (err, items) {
                cat.items = items;
                catList.push(cat);
                cont2();
            });
        }).fin(function (cont2) {//all方法废弃
            cont(null, catList);
        });

    }).then(function(cont,list){
        res.json(list);
    }).fail(function(cont,err){
        return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    });

};


//all the categories
exports.list = function(req, res) {
    var perPage = req.body.s || 20,
        page = req.body.p || 1;

    Category.find().skip((page-1) * perPage).limit(perPage).exec(function (err, categories) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            var pagination = {};
            pagination.total = categories.length;
            pagination.pageSize = perPage;
            pagination.pageIndex = page;

            res.json(resJson(null, categories, pagination));
        }
    });
};


exports.create = function(req, res) {

};
