'use strict';

module.exports = function(app) {
    var misc = require('../../app/controllers/misc.server.controller');
    var categories = require('../../app/controllers/categories.server.controller');
    var ads = require('../../app/controllers/ads.server.controller');

    app.route('/api/schedules')
        .get(misc.scheduleList);

    app.route('/api/categories')
        .get(categories.list)
        .post(categories.create);

    app.route('/api/ads')
        .get(ads.list)
        .post(ads.create);


    //app.route('/api/categories/:id')
    //	.get(categories.read)
    //	.put(categories.update)
    //	.delete(categories.delete);
};
