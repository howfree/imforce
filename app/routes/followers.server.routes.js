'use strict';

/**
 * Module dependencies.
 */
var followers = require('../../app/controllers/followers.server.controller');

module.exports = function(app) {
    // Routes
    app.route('/followers')
        .get(followers.list)
        .post(followers.create);

    app.route('/followers/:id')
        .get(followers.read)
        .put(followers.update)
        .delete(followers.delete);

    // Finish by binding the follower middleware
    app.param('followerId', followers.followerByID);
};
