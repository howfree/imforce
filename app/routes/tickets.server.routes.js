'use strict';

/**
 * Module dependencies.
 */
var tickets = require('../../app/controllers/tickets.server.controller');

module.exports = function(app) {
    // Routes
    app.route('/tickets')
        .get(tickets.list)
        .post(tickets.create);

    app.route('/tickets/:id')
        .get(tickets.read)
        .put(tickets.update)
        .delete(tickets.delete);

    // Finish by binding the follower middleware
    //app.param('id', tickets.ticketByID);
};
