'use strict';
/**
 * Module dependencies.
 */
var im = require('../../app/controllers/im.server.controller');


module.exports = function(app) {
    //listen to wechat client
    app.route('/im')
        .all(im.inbound);

    //fecth all followers into local db.
    app.route('/im/followers/fetch')
        .get(im.fetchFollowers);
    //fetch groups and save new ones.
    app.route('/im/groups/fetch')
        .get(im.fetchGroups);
};
