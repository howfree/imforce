'use strict';

module.exports = function(app) {
	// Root routing
	var core = require('../../app/controllers/core.server.controller');
	app.route('/').get(function(req,res){
		if(req.user){
			res.redirect('/dashboard');
		}
		else{
			core.index(req,res);
		}
	});

	//after logging, goto dashboard.
	app.route('/dashboard').get(core.index);
};
