'use strict';

/**
 * Module dependencies.
 */
var messages = require('../../app/controllers/messages.server.controller');

module.exports = function(app) {
    //  Routes
    app.route('/messages')
        .get(messages.list)
        .post(messages.create);

    //app.route('/messages/:id')
    //    .get(messages.read)
    //    .put(messages.update)
    //    .delete(messages.delete);

};
