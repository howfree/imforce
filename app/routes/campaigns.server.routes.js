'use strict';

/**
 * Module dependencies.
 */
var campaigns = require('../../app/controllers/campaigns.server.controller');

module.exports = function(app) {
    // Routes
    app.route('/campaigns')
        .get(campaigns.list)
        .post(campaigns.create);

    app.route('/campaigns/:id')
        .get(campaigns.read)
        .put(campaigns.update)
        .delete(campaigns.delete);
};
