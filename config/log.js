'use strict';

/**
 * Module dependencies.
 */

var config = require('./config');
var fs = require('fs');
var log4js = require('log4js');


log4js.configure({
	appenders: [
		{
			type: 'console',
			category: 'console'
		},
		{
			type: 'dateFile',
			filename: 'logs/app.log',
			pattern: 'YYYY-MM-DD',
			alwaysIncludePattern: false,
			category: 'dateFileLog',
			maxLogSize: 10485760,
			numBackups: 3
		}
	],
	replaceConsole: true,
	levels:{
		consoleLog: config.log.level,
		dateFileLog: config.log.level
	}
});

var dateFileLog = log4js.getLogger('dateFileLog');

exports.use = function(app) {
	//页面请求日志,用auto的话,默认级别是WARN
	//app.use(log4js.connectLogger(dateFileLog, {level:'auto', format:':method :url'}));
	app.use(log4js.connectLogger(dateFileLog, {level:config.log.level, format:':method :url'}));
};

exports.getLogFormat = function() {
	return config.log.format;
};

exports.getLogOptions = function() {
	var options = {};

	try {
		if ('stream' in config.log.options) {
			options = {
				stream: fs.createWriteStream(process.cwd() + '/logs/' + config.log.options.stream, {flags: 'a'})
			};
		}
	} catch (e) {
		options = {};
	}

	return options;
};

exports.logger = dateFileLog;
