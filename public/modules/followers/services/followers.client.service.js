'use strict';

// service used for communicating with the REST endpoints
angular.module('followers').factory('Followers', ['$resource',
    function($resource) {
        return $resource('/followers/:id',{
            id: '@_id'
        },{
            getByImId:{method:'GET',params:{imId:''}}
        });
    }
]);
