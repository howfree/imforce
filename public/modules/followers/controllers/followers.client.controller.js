/**
 * Created by lastsky on 12/4/14.
 */
'use strict';
angular
    .module('followers')
    .controller('FollowersController', ['$stateParams', '$scope', '$resource', 'Followers', 'app', function ($stateParams, $scope, $resource, Followers, app) {

        console.log($stateParams);
        $scope.menus = [{name:'follower list',action:'/followers',isActive:true}];

        function load(){
            var params = {
                p: $stateParams.p || 1,
                s: $stateParams.s || app.myConf.pageSize(null, 'common', 20),
                groupId : $stateParams.groupId
            };

            app.promiseGet(params, Followers, 'follower', app.cache.follower).then(function (data) {
                var pagination = data.pagination || {};

                //pagination.pageSize = app.myConf.pageSize(pagination.pageSize, 'index');

                $scope.pagination = pagination;

                $scope.followers = data.data;
            });
        }


        $scope.activeOperation = '';

        $scope.toggleAdd = function () {
            //change its style from default to primary
            if ($scope.activeOperation === 'adding') {

                var follower = new Followers({
                    imId: this.new_ID,
                    nickname: this.new_nickname,
                    sex: this.new_sex,
                    city: this.new_city,
                    province: this.new_province,
                    country: this.new_country,
                    remark: this.new_remark
                });

                follower.$save(function (response) {
                    $scope.followers.push(response);
                    $scope.activeOperation = '';
                }, function (errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            }
            else {
                $scope.activeOperation = 'adding';
            }
            //create new line
        };


        $scope.toggleEdit = function () {
            //change its style from default to primary
            if ($scope.activeOperation === 'editing') {
                $scope.activeOperation = '';
            }
            else {
                $scope.activeOperation = 'editing';
            }
            //create new line
        };

        $scope.toggleRemove = function () {
            //change its style from default to primary
            if ($scope.activeOperation === 'removing') {
                $scope.activeOperation = '';
            }
            else {
                $scope.activeOperation = 'removing';
            }
            //create new line
        };

        $scope.toggleExpand = function (btn) {
            var body = angular.element('body');//$('body')
            //e.preventDefault();
            var box = btn.closest('div.box');
            var button = btn.find('i');
            button.toggleClass('fa-expand').toggleClass('fa-compress');
            box.toggleClass('expanded');
            body.toggleClass('body-expanded');
            var timeout = 0;
            if (body.hasClass('body-expanded')) {
                timeout = 100;
            }
            setTimeout(function () {
                box.toggleClass('expanded-padding');
            }, timeout);
            setTimeout(function () {
                box.resize();
                box.find('[id^=map-]').resize();
            }, timeout + 50);
        };

        // sync data with wechat
        $scope.sync = function () {
            $scope.activeOperation = 'loading';
            var customerResource = $resource('/im/followers/fetch');
            customerResource.query().$promise.then(function (followers) {
                load();
                $scope.activeOperation = '';
            });

        };

        load();

        $scope.goDetail = function(follower){
            var tab = {id:'follower.detail',name: follower.nickName + 'detail',action:'/followers/'+follower._id,isActive:true};
            app.tabs.push(tab);
        };

        $scope.goDial = function(follower){
            var tab = {id:'follower.dial',name: follower.nickName + 'dial',action:'/inbound/'+follower._id,isActive:true};
            app.tabs.push(tab);
        }
    }]);
