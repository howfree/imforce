'use strict';

// Setting up route
angular.module('followers').config(['$stateProvider',
    function ($stateProvider) {
        // Followers state routing
        $stateProvider.
            state('listFollowers', {
                url: '/followers',
                views: {
                    'sidebar':{
                        templateUrl: '/modules/dashboard/views/sidebar.client.view.html'
                    },
                    'content': {
                        controller: 'FollowersController',
                        templateUrl: 'modules/followers/views/main.client.view.html'
                    }
                }
            }).
            state('createFollower', {
                url: '/followers/create',
                templateUrl: 'modules/followers/views/create-follower.client.view.html'
            }).
            state('viewFollower', {
                url: '/followers/:followerId',
                templateUrl: 'modules/followers/views/view-follower.client.view.html'
            }).
            state('editFollower', {
                url: '/followers/:followerId/edit',
                templateUrl: 'modules/followers/views/edit-follower.client.view.html'
            });
    }
]);
