'use strict';

// service used for communicating with the REST endpoints
angular.module('campaigns').factory('Campaigns', ['$resource',
    function($resource) {
        return $resource('/campaigns/:campaignId',{
            campaignId: '@_id'
        },{
            update: {
                method: 'PUT'
            }
        },{
            getByImId:{method:'GET',params:{imId:''}}
        });
    }
]);
