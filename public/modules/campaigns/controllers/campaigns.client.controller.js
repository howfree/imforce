/**
 * Created by lastsky on 12/4/14.
 */
'use strict';
angular
    .module('campaigns',['underscore'])
    .controller('CampaignsController', ['$stateParams', '$scope', '$resource', 'Campaigns', 'app', '_', function ($stateParams, $scope, $resource, Campaigns, app, _) {

        function load(){
            var params = {
                p: $stateParams.p || 1,
                s: $stateParams.s || app.myConf.pageSize(null, 'common', 20)
            };

            app.promiseGet(params, Campaigns, 'campaign', app.cache.campaign).then(function (data) {
                var pagination = data.pagination || {};

                //pagination.pageSize = app.myConf.pageSize(pagination.pageSize, 'index');

                $scope.pagination = pagination;

                $scope.campaigns = data.data;
            });
        }


        $scope.activeOperation = '';

        $scope.toggleAdd = function () {
            //change its style from default to primary
            if ($scope.activeOperation === 'adding') {

                var campaign = new Campaigns({
                    name: this.campaign_name,
                    type: this.campaign_type,
                    startDate: this.campaign_startdate,
                    endDate: this.campaign_enddate,
                    description: this.campaign_description,
                    thumbnail: this.campaign_thumbnail,
                    ad: this.campaign_ad
                });

                campaign.$save(function (response) {
                    $scope.campaigns.push(response);
                    $scope.activeOperation = '';
                    toggleExpand();

                }, function (errorResponse) {
                    $scope.error = errorResponse.data.message;
                });


            }
            else {
                $scope.activeOperation = 'adding';
                toggleExpand();
            }
        };


        $scope.toggleEdit = function () {
            //change its style from default to primary
            if ($scope.activeOperation === 'editing') {
                $scope.activeOperation = '';
            }
            else {
                $scope.activeOperation = 'editing';
            }
            //create new line
        };

        $scope.toggleRemove = function () {
            //change its style from default to primary
            if ($scope.activeOperation === 'removing') {
                $scope.activeOperation = '';
            }
            else {
                $scope.activeOperation = 'removing';
            }
            //create new line
        };


        $scope.removeCampaign = function(campaign){
            if (campaign) {
                $scope.campaign = Campaigns.get({
                    campaignId: campaign._id
                },function(){
                    $scope.campaign.$remove();
                });


                for (var i in $scope.campaigns) {
                    if ($scope.campaigns[i] === campaign) {
                        $scope.campaigns.splice(i, 1);
                    }
                }
            } else {
                $scope.campaign.$remove(function() {
                    $location.path('campaigns');
                });
            }

        };

        function toggleCompress(){

        }

        $scope.toggleExpand = function(){
            toggleExpand();
            $scope.activeOperation ='';
        };

        function toggleExpand() {
            var body = angular.element('body');//$('body')
            var btn = angular.element('#newcampaign');
            //e.preventDefault();
            var box = btn.closest('div.box');
            var button = btn.find('i');
            button.toggleClass('fa-expand').toggleClass('fa-compress');
            box.toggleClass('expanded');
            body.toggleClass('body-expanded');
            var timeout = 0;
            if (body.hasClass('body-expanded')) {
                timeout = 100;
            }
            setTimeout(function () {
                box.toggleClass('expanded-padding');
            }, timeout);
            setTimeout(function () {
                box.resize();
                box.find('[id^=map-]').resize();
            }, timeout + 50);
        };


        load();

        $scope.goDetail = function(campaign){

            //var tab = {name: follower.nickName + 'detail',action:'/campaigns/'+follower._id,isActive:true};

            //app.tabs.push(tab);
        };

        $scope.goDial = function(campaign){
            //var tab = {name: follower.nickName + 'dial',action:'/inbound/'+follower._id,isActive:true};
            //app.tabs.push(tab);
        }
    }]);
