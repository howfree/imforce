'use strict';
angular
    .module('inbound')
    .controller('SingleInController', ['$scope', '$stateParams', '$compile', 'Messages', 'Followers', 'Tickets', 'app', function ($scope, $stateParams, $compile, Messages, Followers, Tickets, app) {
        console.log('single inbound');
        //current follower Id.
        $scope.currentId = $stateParams.Id;
        $scope.currentTab = 'profileTab';

        var unreadStatus = 0;

        function init() {
            Followers.get({
                id: $stateParams.id
            }).$promise.then(function (follower, err) {

                    $scope.follower = follower;
                    $scope.currentImId = follower.imId;
                    if (follower) {
                        //retrieve unread messages from this follower.
                        Messages.getUnreadList({
                            status: unreadStatus,
                            fromUserId: $scope.follower.imId
                        }).$promise.then(function (unReadList, err) {
                            if (err) {
                                $scope.messages = '';
                            }
                            $scope.messages = unReadList.data;

                        });

                        var params = {
                            p: $stateParams.p || 1,
                            s: $stateParams.s || app.myConf.pageSize(null, 'common', 20)
                        };

                        app.promiseGet(params, Tickets, 'ticket', app.cache.ticket).then(function (data) {
                            var pagination = data.pagination || {};

                            //pagination.pageSize = app.myConf.pageSize(pagination.pageSize, 'index');

                            $scope.pagination = pagination;

                            $scope.tickets = data.data;
                        });

                    }
                });

        }

        //send message to specific follower
        $scope.send = function(){
            var message = new Messages({
                toUserId: $scope.currentImId,
                msgType: 'text',
                content:$scope.textContent
            });

            message.$save(function (message) {
                $scope.messages.push(message);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });

        };

        init();

        //related chat history for creating ticket.
        $scope.selectedMessages = [];
        $scope.selectedMessageIds = [];
        $scope.selectMessage = function(message){
            $scope.selectedMessages.push(message);
            $scope.selectedMessageIds.push(message._id);
        };

        //ticket type reference
        $scope.types = [
            {name:'question', value:0},
            {name:'order', value:1},
            {name:'problem', value:2},
            {name:'ticket', value:3},
            {name:'others', value:4}
        ];
        //defulat ticket type
        //$scope.newticket.type = $scope.types[0]; //

        //
        $scope.createTicket = function(){
            var ticket = new Tickets({
               subject: $scope.newticket.subject,
               type: $scope.newticket.type,
               content: $scope.newticket.content,
               messages: $scope.selectedMessageIds
            });
            ticket.$save(function(ticket){
                $scope.newticket = null;
                $scope.selectedMessageIds = null;
                $scope.selectedMessages = null;
                $scope.result = 'success';
            },function(errorResponse){
                $scope.error = errorResponse.data.message;
            });

        };

        $scope.switchMessage = function(){
            setCurrentTab('messageTab');
        };

        $scope.switchTicket = function(){
            setCurrentTab('ticketTab');
        };

        $scope.switchProfile = function(){
            setCurrentTab('profileTab');
        };

        $scope.switchSetting = function(){
            setCurrentTab('settingTab');
        };

        function setCurrentTab(currentTab){
            $scope.currentTab = currentTab;
        }

        $scope.autoResource = {
            options: {
                html: true,
                focusOpen: true,
                onlySelectValid: true,
                source: function (request, response) {
                    var data = [
                        "Asp",
                        "BASIC",
                        "C",
                        "C++",
                        "Clojure",
                        "COBOL",
                        "Scheme"
                    ];
                    data = $scope.autoResource.methods.filter(data, request.term);

                    if (!data.length) {
                        data.push({
                            label: 'not found',
                            value: ''
                        });
                    }
                    // add "Add Language" button to autocomplete menu bottom
                    data.push({
                        label: $compile('<a class="btn btn-link ui-menu-add" ng-click="addLanguage()">Add Language</a>')($scope),
                        value: ''
                    });
                    response(data);
                }
            },
            methods: {}
        };

        $scope.toggleExpand = function(){
            toggleExpand();
        };

        function toggleExpand() {
            var body = angular.element('body');//$('body')
            var btn = angular.element('#chatbox');
            //e.preventDefault();
            var box = btn.closest('div.box');
            var button = btn.find('i');
            button.toggleClass('fa-expand').toggleClass('fa-compress');
            box.toggleClass('expanded');
            body.toggleClass('body-expanded');
            var timeout = 0;
            if (body.hasClass('body-expanded')) {
                timeout = 100;
            }
            setTimeout(function () {
                box.toggleClass('expanded-padding');
            }, timeout);
            setTimeout(function () {
                box.resize();
                box.find('[id^=map-]').resize();
            }, timeout + 50);
        };
    }]);
