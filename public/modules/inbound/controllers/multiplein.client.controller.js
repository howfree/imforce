'use strict';
angular
    .module('inbound')
    .controller('MultipleInController', ['$scope', 'Messages', 'Followers',function ($scope, Messages, Followers) {
        $scope.currentImId='';
        $scope.init = function () {
            Followers.get(function(followers){
                $scope.followers=followers.data;
            });
        };


        //choose a follower then chat.
        $scope.chat = function (imId) {
            //specify current follower.
            $scope.currentImId= imId;

            //retrieve unread messages from this follower.
            Messages.getUnreadList({status:0,fromUserId:imId}).$promise.then(function(unReadList,err){
                if(err){
                    $scope.messages = '';
                }
                $scope.messages = unReadList.data;

            });

        };

        $scope.init();

        //send message to specific follower
        $scope.send = function(){
            var message = new Messages({
                toUserId: $scope.currentImId,
                msgType: 'text',
                content:$scope.textContent
            });

            message.$save(function (message) {
                console.log(message);
                $scope.messages.push(message);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });

        };
    }]);
