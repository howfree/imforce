'use strict';
angular.module('inbound').config(['$stateProvider',
    function ($stateProvider) {
        $stateProvider
            .state('inbound', {
                url: '/inbound',
                controller: 'MultipleInController',
                templateUrl: '/modules/inbound/views/multiplein.client.view.html'
            }).state('singleIn', {
                url: '/inbound/:id',
                views : {
                    'inbound': {
                        controller: 'SingleInController',
                        templateUrl: '/modules/inbound/views/singlein.client.view.html'
                    }
                }
            });

    }
]);
