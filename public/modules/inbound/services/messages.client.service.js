'use strict';

//service used for communicating with the REST endpoints
angular.module('inbound').factory('Messages', ['$resource',
    function($resource) {
        return $resource('/messages/:id',{
            id: '@_id'
        },{
            getUnreadList:{method:'GET', params:{status:'',fromUserId:'',direction:''}},
            getByFollowerId:{method:'GET',params:{fromUserId:''}}
        });
    }
]);
