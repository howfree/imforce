/**
 * Created by lastsky on 12/4/14.
 */
'use strict';
angular
    .module('categories')
    .controller('CategoriesController', ['$stateParams', '$scope', '$resource', 'Ads', 'app', function ($stateParams, $scope, $resource, Ads, app) {
        load();

        function load(){
            var params = {
                p: $stateParams.p || 1,
                s: $stateParams.s || app.myConf.pageSize(null, 'common', 20)
            };

            app.promiseGet(params, Ads, 'ad', app.cache.ad).then(function (data) {
                var pagination = data.pagination || {};

                //pagination.pageSize = app.myConf.pageSize(pagination.pageSize, 'index');

                $scope.pagination = pagination;
                $scope.ads = data.data;
            });
        }

    }]);
