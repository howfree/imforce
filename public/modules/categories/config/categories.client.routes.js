'use strict';

// Setting up route
angular.module('categories').config(['$stateProvider',
    function ($stateProvider) {
        // Followers state routing
        $stateProvider.
            state('listCategories', {
                url: '/categories',
                views: {
                    'sidebar':{
                        templateUrl: '/modules/dashboard/views/sidebar.client.view.html'
                    },
                    'content': {
                        controller: 'CategoriesController',
                        templateUrl: 'modules/categories/views/categories.client.view.html'
                    }
                }
            });
    }
]);
