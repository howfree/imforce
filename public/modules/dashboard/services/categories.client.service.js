'use strict';

// service used for communicating with the REST endpoints
angular.module('dashboard').factory('Categories', ['$resource',
    function($resource) {
        return $resource('api/categories/:id',{
            id: '@_id'
        },{
            getByParentId:{method:'GET',params:{parentId:''},isArray:true}
        });
    }
]);
