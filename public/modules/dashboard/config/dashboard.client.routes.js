'use strict';

// Setting up route
angular.module('dashboard').config(['$stateProvider',
    function($stateProvider) {
        // state routing
        $stateProvider.
            state('dashboard', {
                url: '/dashboard',
                controller:'DashBoardController',
                templateUrl: '/modules/dashboard/views/dashboard.client.view.html'
            }).
            state('dashboard.default', {
                url: '/dashboard/default',
                views : {
                    'content' : {
                        templateUrl : '/modules/dashboard/views/default.client.view.html'
                    }
                }
            }).
            state('dashboard.follower', {
                url: '/followers',
                views : {
                    'content' : {
                        templateUrl : '/modules/followers/views/main.client.view.html',
                        controller : 'FollowersController'
                    }
                }
            }).
            state('dashboard.faq', {
                url: '/faqs',
                templateUrl: '/modules/dashboard/views/list-faqs.client.view.html'
            }).
            state('dashboard.ticket', {
                url: '/tickets',
                views : {
                    'content' : {
                        templateUrl: '/modules/tickets/views/tickets.client.view.html',
                        controller : 'TicketsController'
                    }
                }
            }).
            state('dashboard.category', {
                url: '/categories',
                views : {
                    'content' : {
                        templateUrl: '/modules/categories/views/categories.client.view.html',
                        controller : 'CategoriesController'
                    }
                }
            }).
            state('dashboard.campaign', {
                url: '/campaigns',
                views : {
                    'content' : {
                        templateUrl: '/modules/campaigns/views/campaigns.client.view.html',
                        controller : 'CampaignsController'
                    }
                }
            });
    }
]);
