'use strict';

angular.module('ads')
.directive('adForm',adForm);

function adForm(){
    return {
      restrict: 'A',
      templateUrl: '/modules/ads/views/adform.client.view.html',
      link: function(scope, el, attrs){
          console.log("adForm Directive Loaded");
      }

    };
}
