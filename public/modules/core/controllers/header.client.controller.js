'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$http', '$location', '$timeout', 'app', 'Authentication', 'Messages',
	function($scope, $http, $location, $timeout, app, Authentication, Messages) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.messageCount=0;
		$scope.followerMessages=[];
		$scope.taskCount = 0;
		$scope.noteCount = 0;
		$scope.tabs = app.tabs;

		function loadUnreadMessages(){
			Messages.getUnreadList({status:0,direction:'0'}).$promise.then(function(unReadList){

				var messages =unReadList.data;
				$scope.messageCount = messages.length;

				//group by fromUserId.
				// [{fromUserId1:{count:'',fromUser:{}}},fromUserId2:{count:'',fromUser:{}}]
				var i, row, map={},followerMessages= [];
				for (i = 0; i < messages.length; i++) {
					row = messages[i];
					map[row.fromUserId] = map[row.fromUserId] || {count: 0};
					if (map[row.fromUserId].fromUser === undefined) {
						map[row.fromUserId].fromUser = row._fromUser;
						map[row.fromUserId].count += 1;
					}
					else{
						map[row.fromUserId].count += 1;
					}

				}

				//
				for(var k in map){
					followerMessages.push(map[k]);
				}
				$scope.followerMessages= followerMessages;

				// check for changes
				//$timeout(loadUnreadMessages, 5000);
			});

		}

		loadUnreadMessages();

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		$scope.signout = function(){
			$http.get('/auth/signout', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = null;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

	}
]);
