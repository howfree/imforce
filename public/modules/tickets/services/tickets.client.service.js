'use strict';

// service used for communicating with the REST endpoints
angular.module('tickets').factory('Tickets', ['$resource',
    function($resource) {
        return $resource('/tickets/:id',{
            id: '@_id'
        },{
            getByImId:{method:'GET',params:{imId:''}}
        });
    }
]);
