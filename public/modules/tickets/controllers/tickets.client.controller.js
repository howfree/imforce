'use strict';

angular.module('tickets').controller('TicketsController', ['$scope', '$stateParams', '$location', 'app', 'Tickets',
    function($scope, $stateParams, $location, app, Tickets) {
        console.log($stateParams);

        function load(){
            var params = {
                p: $stateParams.p || 1,
                s: $stateParams.s || app.myConf.pageSize(null, 'common', 20),
                status : $stateParams.status
            };

            app.promiseGet(params, Tickets, 'ticket', app.cache.ticket).then(function (data) {
                var pagination = data.pagination || {};

                //pagination.pageSize = app.myConf.pageSize(pagination.pageSize, 'index');

                $scope.pagination = pagination;

                $scope.tickets = data.data;
            });
        }


        load();
    }]
);
