'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName)
    .constant('app', {
        version: Date.now()
    })
    .config(['$locationProvider', '$httpProvider',
        function ($locationProvider,$httpProvider) {
            $locationProvider.html5Mode(true);

            // global loading status
            var count = 0,
                loading = false,
                status = {
                    count: 0,
                    total: 0
                };
        }
    ])
    .run(['$rootScope', '$q', '$location', '$timeout', '$filter', '$locale', 'cache', 'promiseGet', 'myConf', 'tools', 'app', 'timing', function ($rootScope, $q, $location, $timeout, $filter, $locale, cache, promiseGet, myConf, tools, app, timing) {
        app.tabs = [];
        app.q = $q;
        app.timing = timing;
        app.location = $location;
        app.timeout = $timeout;
        app.timeOffset = 0;
        app.timestamp = Date.now() + 0;
        app.filter = $filter;
        app.locale = $locale;
        app.cache = cache;
        app.promiseGet = promiseGet;
        app.myConf = myConf;
        app.rootScope = $rootScope;
        angular.extend(app, tools);


        var global = $rootScope.global = {
            isAdmin: false,
            isEditor: false,
            isLogin: false,
            info: {}
            },
            appWin = $(window);

        timing(function () { // 保证每360秒内与服务器存在连接，维持session
            if (Date.now() - app.timestamp - app.timeOffset >= 240000) {
                //init();
            }
        }, 120000);

        app.checkUser = function () {
            global.isLogin = !! global.user;
            global.isAdmin = global.user && global.user.role === 5;
            global.isEditor = global.user && global.user.role >= 4;
        };

        app.clearUser = function () {
            global.user = null;
            app.checkUser();
        };

    }]);

//Then define the init function for starting up the application
angular.element(document).ready(function () {
    //Fixing facebook bug with redirect
    //if (window.location.hash === '#_=_') window.location.hash = '#!';
    //Then init the app
    angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
